SPIDER_DIR=spider
CONFSITE_SRC=$(SPIDER_DIR)/confsite
LANYARD_SRC=$(SPIDER_DIR)/lanyard
WIKI_SRC=$(SPIDER_DIR)/wiki

SCHEDULE_DIR=schedule
VENUE_DIR=venues
PWD=`pwd`
# Note: xsltproc param values are XPath expressions, so strings must be properly quoted when evaluated.
CSS_PARAM=\'../css/code4lib.css\'

all: download sched

download: grab-confsite grab-wiki

grab-confsite:
	mkdir -p $(CONFSITE_SRC) && \
	cd $(CONFSITE_SRC) && \
	wget --wait=2 --timestamping --no-host-directories \
	--include /conference/2013 --cut-dirs=2 \
	-r http://code4lib.org/conference/2013/

grab-wiki:
	mkdir -p $(WIKI_SRC) && \
	wget -O $(WIKI_SRC)/2013_preconference_proposals.html http://wiki.code4lib.org/index.php/2013_preconference_proposals

# grab-lanyard:
# 	mkdir -p $(LANYARD_SRC) && \
# 	cd $(LANYARD_SRC) && \
# 	wget --wait=2 --timestamping --no-host-directories \
# 	--include /2013/c4l13 --cut-dirs=2 \
# 	--exclude-directories=/2013/c4l13/add-session,/2013/c4l13/attendees,/2013/c4l13/on,/2013/c4l13/save-to-calendar,/2013/c4l13/trackers \
# 	--reject '*feedback*' \
# 	-r http://lanyrd.com/2013/c4l13/

sched:
	for f in $(CONFSITE_SRC)/* ; do \
		if test `basename $$f` = index.html; \
		then continue; \
		fi; \
		xsltproc --html --param css $(CSS_PARAM) xslt/c4l2guide.xslt $$f > $(SCHEDULE_DIR)/`basename $$f`.html ; \
	done
	xsltproc --html --param css $(CSS_PARAM) xslt/wiki2guide.xslt $(WIKI_SRC)/2013_preconference_proposals.html > $(SCHEDULE_DIR)/2013_preconference_proposals.html
	cd $(SCHEDULE_DIR) && mv schedule.html index.html
