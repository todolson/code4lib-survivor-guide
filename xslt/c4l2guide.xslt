<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    exclude-result-prefixes="xd"
    version="1.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Feb 1, 2013</xd:p>
            <xd:p><xd:b>Author:</xd:b> tod</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:output method="html"/>
    
    <xsl:param name="css" select="'/css/code4lib.css'"/>
    <xsl:variable name="confpath" select="'/conference/2013'"/>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
    <!-- short-circuit the table layout -->
    <!-- using @id rather than id() out of uncertainly -->
    <xsl:template match="table[@id='content']">
        <xsl:apply-templates select="tr/td[@id='main']/child::*"/>
    </xsl:template>

    <!-- Remove header, sidebar, navlinks, etc. -->
    <xsl:template match="id('header')"/>
    <xsl:template match="id('sidebar-right')"/>
    <xsl:template match="div[@class='links']"/>
    <xsl:template match="div[@class='navlinks']"/>
    <xsl:template match="id('comment')"/>
    
    <!-- Fix up CSS -->
    <xsl:template match="style"/>
    <xsl:template match="link[@rel='stylesheet']">
        <link rel="stylesheet" type="text/css" href="{$css}" />
    </xsl:template>

    <!-- Fix up anchor elements -->
    <xsl:template match="a[starts-with(@href,'/conference/2013')]">
        <xsl:element name="a">
            <xsl:attribute name="href">
                <xsl:value-of select="concat(substring-after(@href,'/conference/2013/'),'.html')"/>
            </xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="a[starts-with(@href, 'http://wiki.code4lib.org/index.php/2013_preconference_proposals')]">
        <xsl:variable name="proposal_base" select="'2013_preconference_proposals'"/>
        <xsl:variable name="proposal_dir" select="'http://wiki.code4lib.org/index.php'"/>
        <xsl:variable name="proposal_url" select="concat($proposal_dir, '/', $proposal_base)"></xsl:variable>
        <xsl:element name="a">
            <xsl:attribute name="href">
                <xsl:value-of select="concat($proposal_base, '.html', substring-after(@href, $proposal_url))"/>
            </xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
    
    <!-- Removed repeat of title -->
    <!-- Nervous-making test... postion() would be better if there's a way -->
    <!--<xsl:template match="p[child::strong and parent::div[@class='content']]"/>-->
        
    
</xsl:stylesheet>